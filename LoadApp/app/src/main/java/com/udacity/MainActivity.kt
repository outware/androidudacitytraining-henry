package com.udacity

import android.app.DownloadManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {

    private var downloadID: Long = 0

    private lateinit var notificationManager: NotificationManager
    private lateinit var pendingIntent: PendingIntent
    private lateinit var action: NotificationCompat.Action

    private var downloadUrl: String? = null
    private var downloadName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //Receiver that receives broadcast from download manager when download is complete
        registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))

        createNotificationChannel()

        custom_button.setOnClickListener {
            //If downloadUrl is not null, we can download, otherwise prompt user to select an option
            downloadUrl?.let { download() } ?: Toast.makeText(
                this,
                "Select an option",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    //Receiver for DownloadManager broadcase
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            val action = intent?.action

            //If this download is the one we want
            if (downloadID == id) {
                //Check status of the download
                if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                    val query = DownloadManager.Query()
                    query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
                    val manager =
                        context!!.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                    val cursor: Cursor = manager.query(query)
                    if (cursor.moveToFirst()) {
                        if (cursor.count > 0) {
                            val status =
                                cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                            if (status == DownloadManager.STATUS_SUCCESSFUL) {
                                custom_button.setLoadingState(ButtonState.Completed)
                                notificationManager.sendNotification(
                                    downloadName!!,
                                    applicationContext,
                                    "Success"
                                )
                            } else {
                                custom_button.setLoadingState(ButtonState.Completed)

                                //Bring to new screen with failed status on click of notification
                                notificationManager.sendNotification(
                                    downloadName!!,
                                    applicationContext,
                                    "Failed"
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    //Called when download button is pressed
    private fun download() {

        val request =
            DownloadManager.Request(Uri.parse(downloadUrl))
                .setTitle(getString(R.string.app_name))
                .setDescription(getString(R.string.app_description))
                .setRequiresCharging(false)
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)

        val downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        downloadID =
            downloadManager.enqueue(request)// enqueue puts the download request in the queue.
        //When downloadManager completes it sends a broadcast

        //Switch to downloading
        custom_button.setLoadingState(ButtonState.Loading)
    }

    //This is called by the view as an onClick attribute
    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.download_glide_button ->
                    if (checked) {
                        downloadUrl = GLIDE_URL
                        downloadName = getString(R.string.download_glide_text)
                    }
                R.id.download_loadapp_button ->
                    if (checked) {
                        downloadUrl = UDACITY_URL
                        downloadName = getString(R.string.download_loadapp_text)
                    }
                R.id.download_retrofit_button ->
                    if (checked) {
                        downloadUrl = RETROFIT_URL
                        downloadName = getString(R.string.download_retrofit_text)
                    }
            }
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channelId = getString(R.string.download_notification_channel_id)
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    //Github download object
    companion object {
        private const val UDACITY_URL =
            "https://github.com/udacity/nd940-c3-advanced-android-programming-project-starter/archive/master.zip"
        private const val GLIDE_URL = "https://github.com/bumptech/glide"
        private const val RETROFIT_URL = "https://github.com/square/retrofit"
    }

}
