package com.udacity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class DetailActivity : AppCompatActivity() {

    private var fileName = ""
    private var status = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)

        ok_button.setOnClickListener {
            toMainActivity()
        }

        // TODO is there a better way to do this?
        //Get starting info from the intent that started this activity
        fileName = intent.getStringExtra("fileName").toString()
        status = intent.getStringExtra("status").toString()
        file_name_value.text = fileName
        status_value.text = status
        if (status == "Success"){
            status_value.setTextColor(Color.GREEN)
        }else{
            status_value.setTextColor(Color.RED)
        }


    }

    //Return user to main activity
    private fun toMainActivity(){
        val mainActivity = Intent(this, MainActivity::class.java)
        startActivity(mainActivity)

        // TODO do l need to clean up the current activity?
    }
}
