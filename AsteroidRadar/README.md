# Project 2 - The Asteroid Radar!  ☄️ 
 
Asteroid Radar is an app to view the asteroids detected by NASA that pass near Earth, you can view all the detected asteroids given a period of time with data such as the size, velocity, distance to earth and if they are potentially hazardous. In this project, you will apply the skills such as fetching data from the internet, saving data to a database, and display the data in a clear, compelling UI.

You will need the NEoWs API which is a free, open source API provided by NASA JPL Asteroid team, as they explain it: “Is a RESTful web service for near earth Asteroid information. With NeoWs a user can: search for Asteroids based on their closest approach date to Earth, lookup a specific Asteroid with its NASA JPL small body id, as well as browse the overall data-set.”

The resulting output of the project will be two screens: a Main screen with a list of all the detected asteroids and a Details screen that is going to display the data of that asteroid once it´s selected in the Main screen list. The main screen will also show the NASA image of the day to make the app more striking. 

![Asteroid Radar](https://video.udacity-data.com/topher/2020/June/5edeac1d_screen-shot-2020-06-08-at-2.21.53-pm/screen-shot-2020-06-08-at-2.21.53-pm.png)
![Asteroid Radar](https://video.udacity-data.com/topher/2020/June/5edeac35_screen-shot-2020-06-08-at-2.22.18-pm/screen-shot-2020-06-08-at-2.22.18-pm.png)

### Getting Started ###

#### Steps ####

The first step is to get an API Key from NASA. Follow the instructions as listed.

1. Got to the following URL - https://api.nasa.gov/ and scroll down a little and you are going to see this:

![API](https://video.udacity-data.com/topher/2020/June/5edeae02_screen-shot-2020-06-08-at-2.30.21-pm/screen-shot-2020-06-08-at-2.30.21-pm.png)

Fill the required fields, click the Signup button and you will get a API key (the API Key is also going to be sent to your email ).

2. You will use the API key to send requests to NASA servers to get data about asteroids - to view how it works, scroll down a little more until you see the NeoWs data.

    Next you are going to see something like this:

![API](https://video.udacity-data.com/topher/2020/June/5edeaece_screen-shot-2020-06-08-at-2.33.10-pm/screen-shot-2020-06-08-at-2.33.10-pm.png)


3. You are now ready to build a query string with the API like the following, using the API_KEY, START_DATE and END_DATE parameters. 
`https://api.nasa.gov/neo/rest/v1/feed?start_date=START_DATE&end_date=END_DATE&api_key=YOUR_API_KEY`. For eg., the following is the demo key that you can try out https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key=DEMO_KEY

The response will be a JSON object like the following:   
  
```

{
  "links": {
    "next": "http://www.neowsapp.com/rest/v1/feed?start_date=2015-09-08&end_date=2015-09-09&detailed=false&api_key=DEMO_KEY",
    "prev": "http://www.neowsapp.com/rest/v1/feed?start_date=2015-09-06&end_date=2015-09-07&detailed=false&api_key=DEMO_KEY",
    "self": "http://www.neowsapp.com/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&detailed=false&api_key=DEMO_KEY"
  },
  "element_count": 24,
  "near_earth_objects": {
    "2015-09-08": [
      {
        "links": {
          "self": "http://www.neowsapp.com/rest/v1/neo/3726710?api_key=DEMO_KEY"
        },
        "id": "3726710",
        "neo_reference_id": "3726710",
        "name": "(2015 RC)",
        "nasa_jpl_url": "http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=3726710",
        "absolute_magnitude_h": 24.3,
        "estimated_diameter": {
          "kilometers": {
            "estimated_diameter_min": 0.0366906138,
            "estimated_diameter_max": 0.0820427065
          },
          "meters": {
            "estimated_diameter_min": 36.6906137531,
            "estimated_diameter_max": 82.0427064882
          },
          "miles": {
            "estimated_diameter_min": 0.0227984834,
            "estimated_diameter_max": 0.0509789586
          },
          "feet": {
            "estimated_diameter_min": 120.3760332259,
            "estimated_diameter_max": 269.1689931548
          }
        },
        "is_potentially_hazardous_asteroid": false,
        "close_approach_data": [
          {
            "close_approach_date": "2015-09-08",
            "close_approach_date_full": "2015-Sep-08 09:45",
            "epoch_date_close_approach": 1441705500000,
            "relative_velocity": {
              "kilometers_per_second": "19.4850295284",
              "kilometers_per_hour": "70146.106302123",
              "miles_per_hour": "43586.0625520053"
            },
            "miss_distance": {
              "astronomical": "0.0269230459",
              "lunar": "10.4730648551",
              "kilometers": "4027630.320552233",
              "miles": "2502653.4316094954"
            },
            "orbiting_body": "Earth"
          }
        ],
        "is_sentry_object": false
      },
... 
```

In the above JSON object, the asteroids are inside `near_earth_objects` key and all the asteroids for a given date are all listed there.

For this project you will use the following fields:

- `id` (Not for displaying but for using in db)
- `absolute_magnitude`
- `estimated_diameter_max` (Kilometers)
- `is_potentially_hazardous_asteroid`
- `close_approach_data` -> `relative_velocity` -> `kilometers_per_second`
- `close_approach_data` -> `miss_distance` -> `astronomical`

### NASA image of the day ###
    
Finally, to make the app more look more interesting, we are going to display the NASA’s image of the day in Main Screen top, this is an simple image we can get from the next link: `https://api.nasa.gov/planetary/apod?api_key=YOUR_API_KEY`

This is also going to return a simple JSON object of which you just need the “url” key, example:

- `url`: https://apod.nasa.gov/apod/image/2001/STSCI-H-p2006a-h-1024x614.jpg
- `media_type`: The image of the day could be an image or a video, we are using only the image, to know what media type is you have to check the `media_type` field, if this value is “image” you are going to download and use the image, if it’s video you are going to ignore it.
- `title`: The title of the picture, this is going to be used as content description of the image for Talk back.

## Project Instructions ##

You are provided with a [starter code](https://github.com/udacity/nd940-android-kotlin-c2-starter/tree/master/starter) in our github repo, which includes the necessary dependencies and plugins that you need to complete this project.

Included in the starter project are:

- Retrofit library to download the data from the Internet.
- Moshi to convert the JSON data we are downloading to usable data in the form of custom classes.
- Picasso library to download and cache images (You could also use Glide, but we found it has some issues downloading images from this particular API).
- RecyclerView to display the asteroids in a list.

We recommend you following the guidelines seen in the courses, as well as using the components from the Jetpack library:

- ViewModel
- Room
- LiveData
- Data Binding
- Navigation

To start follow the next basic steps:

- Clone the Repo.
- Open the project with Android Studio.
- Run the project and check that it compiles correctly.

#### Summarized tasks ####

The application you will build must:

- Include `Main` screen with a list of clickable asteroids as seen in the provided design using a `RecyclerView` with its adapter. You could insert some fake manually created asteroids to try this before downloading any data.
- Include a Details screen that displays the selected asteroid data once it’s clicked in the Main screen as seen in the provided design. The images in the details screen are going to be provided with the starter code: an image for a potentially hazardous asteroid and another one for the non-hazardous ones, you have to display the correct image depending on the `isPotentiallyHazardous` asteroid parameter. Navigation xml file is already included with starter code.
- Download and parse the data from NASA NeoWS (Near Earth Object Web Service) API. As this response cannot be parsed directly with Moshi, we are providing a method to parse the data “manually” for you, it’s called `parseAsteroidsJsonResult` inside `NetworkUtils` class, we recommend trying for yourself before using this method or at least take a close look at it as it is an extremely common problem in real-world apps. For this response we need retrofit-converter-scalars instead of Moshi, you can check this dependency in `build.gradle` (app) file.
- When asteroids are downloaded, save them in the local database.
- Fetch and display the asteroids from the database and only fetch the asteroids from today onwards, ignoring asteroids before today. Also, display the asteroids sorted by date (Check SQLite documentation to get sorted data using a query).
- Be able to cache the data of the asteroid by using a worker, so it downloads and saves today's asteroids in background once a day when the device is charging and wifi is enabled.
- Download Picture of Day JSON, parse it using Moshi and display it at the top of `Main` screen using Picasso Library. (You can find Picasso documentation here: [https://square.github.io/picasso/](https://square.github.io/picasso/)) You could use Glide if you are more comfortable with it, although be careful as we found some problems displaying NASA images with Glide.
- Add content description to the views: Picture of the day (Use the title dynamically for this), details images and dialog button. Check if it works correctly with talk back.
- Make sure the entire app works without an internet connection.

## Project Specification ##

### RecyclerView: ###
| Criteria     | Meets Specifications    |
| --------|---------|
| Create and reuse views in an Android app using `RecyclerView`  | The app displays a list of asteroids in the Main Screen by using a `RecyclerView`, when tapping an item the app opens Details screen.  |
  
  
### Network: ###
| Criteria     | Meets Specifications    |
| --------|---------|
| Build an application that connects to an internet server to retrieve and display live data | The asteroids displayed in the screens are downloaded from the NASA API |
| Use networking and image best practices to fetch data and images | The NASA image of the day is displayed in the Main Screen |
  
### Database and Caching: ###
| Criteria     | Meets Specifications    |
| --------|---------|
| Create a database to store and access user data over time | The app can save the downloaded asteroids in the database and then display them also from the database. The app filters asteroids from the past |
| Implement offline caching to allow users to interact with online content offline | The app downloads the next 7 days asteroids and saves them in the database once a day using workManager with requirements of internet connection and device plugged in. The app can display saved asteroids from the database even if internet connection is not available |

  
### Accessibility: ###
| Criteria     | Meets Specifications    |
| --------|---------|
| Add talkback and push-button navigation to make an Android app accessible | The app works correctly in talk back mode, it provides descriptions for all the texts and images: Asteroid images in details screen and image of the day. It also provides description for the details screen help button |


## Suggestions to Make Your Project Stand Out! 🎨
- Modify the app to support multiple languages, device sizes and orientations.
- Make the app delete asteroids before today once a day from the same workManager that downloads the asteroids.
- Provide styles for the details screen subtitles and values to make it consistent, and make it look like in the designs.

Happy coding! 🤖 