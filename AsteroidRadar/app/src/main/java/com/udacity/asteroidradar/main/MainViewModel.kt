package com.udacity.asteroidradar.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.udacity.asteroidradar.api.AsteroidApi
import com.udacity.asteroidradar.database.getDatabase
import com.udacity.asteroidradar.domain.Asteroid
import com.udacity.asteroidradar.domain.PictureOfDay
import com.udacity.asteroidradar.repository.AsteroidsRepository
import kotlinx.coroutines.launch

enum class AsteroidApiStatus { LOADING, ERROR, DONE }
enum class FilterAsteroids { TODAY, WEEK, ALL }

class MainViewModel(application: Application) : AndroidViewModel(application) {

    //Persistance
    private val database = getDatabase(application)
    private val asteroidsRepository = AsteroidsRepository(database)

    // The internal MutableLiveData that stores the status of the most recent request
    private val _pictureStatus = MutableLiveData<AsteroidApiStatus>()
    val pictureStatus: LiveData<AsteroidApiStatus>
        get() = _pictureStatus

    private val _pictureOfDay = MutableLiveData<PictureOfDay>()
    val pictureOfDay: LiveData<PictureOfDay>
        get() = _pictureOfDay

    private val _navigateToAsteroidDetail = MutableLiveData<Asteroid>()
    val navigateToAsteroidDetail: LiveData<Asteroid>
        get() = _navigateToAsteroidDetail

    //Initialise asteroids to not be filtered
    private var _asteroidFilter = MutableLiveData(FilterAsteroids.ALL)

    val asteroidList = Transformations.switchMap(_asteroidFilter) {
        when (it) {
            FilterAsteroids.WEEK -> asteroidsRepository.weeksAsteroids
            FilterAsteroids.TODAY -> asteroidsRepository.todaysAsteroids
            else -> asteroidsRepository.allAsteroids
        }
    }

    //Initialise Values from API
    init {
        viewModelScope.launch {
            asteroidsRepository.refreshAsteroids()
            getPictureOfDay()
        }
    }

    fun displayAsteroidDetails(asteroid: Asteroid) {
        //Pass argument for action
        _navigateToAsteroidDetail.value = asteroid
    }

    fun displayAsteroidDetailsComplete() {
        _navigateToAsteroidDetail.value = null
    }

    private fun getPictureOfDay() {
        Log.i("picture", "Getting picture")
        viewModelScope.launch {
            _pictureStatus.value = AsteroidApiStatus.LOADING
            try {
                _pictureOfDay.value = AsteroidApi.retrofitService.getPictureOfDay()
                _pictureStatus.value = AsteroidApiStatus.DONE
            } catch (e: Exception) {
                _pictureStatus.value = AsteroidApiStatus.ERROR
                Log.e("Failed: AsteroidRepFile", "Couldn't get image")
            }
        }
    }

    fun filter(filterLevel: FilterAsteroids) {
        _asteroidFilter.postValue(filterLevel)
    }

    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MainViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel")
        }
    }
}