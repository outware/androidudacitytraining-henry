import kotlin.math.PI

//Classes

//Classes are the blueprint, object is the instance of the class, properties are the characteristics of class, methods are the functionality of the class
//Interfaces are a specification that a class can implement, for instance clean could be implemented by aquarium

//Defining classes - Ideally we would create a new package for each class

//By convention we classes start with capital letter
//It is good practice to declare the variables with the constructor arguments
//Constructor is considered (here) after class declaration
class Aquarium(var length: Int = 100, var width: Int = 20, var height: Int = 40) {

    //Defining custom get function for volume prop
    val volume : Int
        get() {
            return width * height * length / 1000
        }

    //One line equiv with setter added
    var volume2 : Int
        get() = width * height * length / 1000
        //Not 100% on this setter function works
        set(value) {height = (value * 1000) / (width * length)}

    //This is essentially getting a value, so it is better represented as a property as above
    fun volume() = width * height * length / 1000

    //Properties are initialised in the order they are defined
    //Water is only 90% of the volume
    var water = volume * 0.9

    //We can also create custom constructors
    //This is the default constructor
    //We can't combine this constructor with the current one
    constructor(numberOfFish: Int): this(){
        //Specific values for this constructor
        //Allowing us to get aquarium size based on number of fish
        val water: Int = numberOfFish * 2000
        val tank: Double = water + water * 0.1
        height = (tank/ (length * width)).toInt()
    }
}

//We would typically abstract this function into another file
fun buildAquarium(){
    //Instantiate class by calling the constructor of Aquarium
    val myAquarium = Aquarium()
    //Even though it looks like we are accessing the property directly, Kotlin automatically calls the appropriate getter
    //We need {} because under the hood we are calling the get function
    //Also, if you press enter inside quotes, it automatically adds a + to improve formatting
    println("Length: ${myAquarium.length}" +
            "Height: ${myAquarium.height}" +
            "Width: ${myAquarium.width}")

    //We can change props, think this automatically uses setter?
    myAquarium.height = 80

    println("Volume: ${myAquarium.volume}")

    val smallAquarium = Aquarium(length = 20, width = 15, height = 30)
    println("Small aquarium ${smallAquarium.volume} litres")

    val smallAquarium2 = Aquarium(numberOfFish = 30)
    println(smallAquarium2)
}

buildAquarium()

//Package visibility
//Defaults to public, all of your variables and classes can be accessed everywhere
//private limits visibility to the file, internal limits visibility to the Module

//Class Visibility
//Public is again the default.
//Private is accessible inside the class, subclasses cannot see
//Protected is accessible inside class, subclasses can see
//Internal - Module

//Example class
class SimpleSpice(){
    val name: String = "curry"
    val spiciness: String = "mild"
    //Property with a getter that returns numeric value for spiciness
    val heat: Int
        get() = when(spiciness){
            "mild" -> 5
            else -> -1
        }
}

val simpleSpice = SimpleSpice()
println("${simpleSpice.name} ${simpleSpice.heat}")

//Using init
class Fish(val friendly: Boolean = true, volumeNeeded: Int){

    val size: Int

    //Secondary constructor, must contain a call to the primary constructor using this
    //It's generally bad practice in Kotlin to use secondary constructors in Kotlin
    //Instead use init blocks, if we need more flexibility, often helper functions are a good choice
    constructor() : this(true, 9){
        println("Running secondary constructor")
    }

    //Can put logic in here to initialise our properties
    //Generally not recommended
    init {
        println("init")
        if (friendly) {
            size = volumeNeeded
        } else {
            size = volumeNeeded * 2
        }
    }

    //Can have multiple init blocks and they always run before any secondary constructors
    init {
        println("Constructed fish of size $volumeNeeded final size ${this.size}")
    }
}

fun fishExample(){
    val fish = Fish(true, 20)
    println("is the fish friendly? ${fish.friendly}. It needs volume ${fish.size}")
}

println("* * * * * * * * * * * * * * * * * * * * * ")
fishExample()


//Example class 2
class Spice1(val name: String, val spiciness: String = "mild"){
    //TODO Why does this not work when we use var???
    val heat: Int
        get() {
            return when (spiciness) {
                "mild" -> 1
                "medium" -> 3
                "spicy" -> 5
                "very spicy" -> 7
                "extremely spicy" -> 10
                else -> 0
            }
        }

    init {
        println("Spice properties are: $name, $spiciness with a heat of $heat")
    }
}

//Initialise Spice class
val spices1 = listOf(
    Spice1("curry", "mild"),
    Spice1("pepper", "medium"),
    Spice1("cayenne", "spicy"),
    Spice1("ginger", "mild"),
    Spice1("red curry", "medium"),
    Spice1("green curry", "mild"),
    Spice1("hot pepper", "extremely spicy")
)

//List of spices that are less less than spicy
val spicelist = spices1.filter {spiceElement -> spiceElement.heat < 5}
//Equivalent
val spicelist2 = spices1.filter {it.heat < 5}
for (spice in spicelist) {
    //Prints a reference
    println(spice)
    //Prints a property
    println(spice.name)
    //How can we print all properties?
}

//Helper function for salt
fun makeSalt() = Spice1("Salt", "None")


//Inheritance
//It is not explicitly declared but this inherits from the Any class
//Open is to allow for subclasses
//Looking at Aquarium class for implementation details
open class Aquarium2(var length: Int = 100, var width: Int = 20, var height: Int = 40) {
    //We need to declare open for the variables as well
    open var volume : Int
        get() = width * height * length / 1000
        //Not 100% on this setter function works
        set(value) {height = (value * 1000) / (width * length)}

    open var water = volume * 0.9

    constructor(numberOfFish: Int): this(){
        val water: Int = numberOfFish * 2000
        val tank: Double = water + water * 0.1
        height = (tank/ (length * width)).toInt()
    }
}

//Create a tower aquarium that is a subclass of aquarium
class TowerTank(): Aquarium2() {
    override var water = volume * 0.8
    override var volume: Int
        get() = (width * height * length / 1000 * PI).toInt()
        set(value) {
            height = (value * 1000) / (width * length)
        }
}

//Example inheritance
//! Remember to declare val/var in primary constructor
open class Book(val title: String, val author: String){

    private var currentPage = 0

    //Could we make this inline?
    open fun readPage() {
        currentPage++
    }
}

//Subclass of Book
//Need to declare new vars for subclass
class eBook(title: String, author: String, var format: String = "text"): Book(title, author){
    private var currentWords = 0
    override fun readPage() {
        currentWords += 250
    }
}

//Interfaces
//Abstract classes and interfaces are two classes that cannot be instantiated on their own
//Difference is that abstract classes have constructurs, whilst interfaces cannot have constructor logic
//The big difference is in when and how we use them. Use an interface if we have lots of methods and 1 or 2 default implementations.

//I.e Good interface:
interface AquariumAction{
    fun eat()
    fun jump()
    fun swim(){
        println("swim")
    }
}

//Use an abstract class when we can't use interface
//i.e

//Abstract class - we need to provide subclasses to instatiate this since it is abstract
abstract class AquariumFish: FishAction {
    //There isn't a good default colour so we leave it abstract
    abstract val colour: String
    //Can override
    override fun eat() = println("yum")
}

//Create subclasses of AquariumFish, note that interfaces do not take constructors
class Shark: AquariumFish(), FishAction{
    override val colour = "grey"

    override fun eat(){
        println("Eat fish")
    }
}

class Plecostmus: AquariumFish(), FishAction{
    override val colour = "gold"
    override fun eat(){
        println("Eat algae")
    }
}

//Interface
interface FishAction{
    //Methods in interfaces are called default implementations
    fun eat()
}

//Only fish that implement fishAction can be passed into FeedFish
//This can help keep clearer and more organised code
fun feedFish(fish: FishAction){
    //Make fish eay
    fish.eat()
}

//Again this would be abstracted into a different file to the classes
fun makeFish(){
    val shark = Shark()
    val pleco = Plecostmus()
    println("Shark is ${shark.colour}")

    shark.eat()
}

makeFish()

// Interface delegation: Lets us add featrues to a class via composition - which is where we use an instance of anotehr class as oposed to inheriting it.
//Composition example:

fun delegate(){
    var pleco = Plecostmus()
    println("Fish has colour ${pleco.colour}")
    pleco.eat()
}

interface FishColour {
    val colour: String
}

//This would be a pointless class, it only ever has one thing
//Instead we use the keyword object to instantiate it
//Is this a singleton?
object GoldColour: FishColour {
    override  val colour = "gold"
}

//A second colour
object RedColour: FishColour {
    override val colour = "red"
}

//We can remove inheritance since we get all the functionality from the interfaces - composition
//We also implement fishcolour by deferring all calls to the object goldcolour
//So every time we call the colour property on this class, it will actually call the colour property on goldcolour
//This is interface delegation?
class Plecostmus2: FishAction, FishColour by GoldColour {
    override fun eat(){
        println("eat algae")
    }
}

class PrintingFishAction(val food: String): FishAction{
    override fun eat(){
        println(food)
    }
}

//Similar to above but we can allow for a variety of input colours, defaulting to cold
//FishColour is then implemented by this object
//We can also replace eat by delegating it to another action through fishaction
//This means all it's overrides are handled by interface delegation
class Plecostmus3(fishColour: FishColour = GoldColour):
    FishAction by PrintingFishAction("A lot of algae"),
    FishColour by fishColour

//Interface delegation is quite powerful and should often be used where you might use abstract class in another language
//It lets us use composition to plug in behaviours instead of requiring a lot of subclasses, each specialing in a different type

//Phew! That was a lot to take in...
//Part 9 and 10 of lesson 4 are definitely the most substantial so far

//Implementation examples for abstract and interface:
//Think it's just adding colour property which is of type Spicecolour, then the interface spicecolour is filled by this
abstract class Spice (val name: String, val spiciness: String = "mild", colour: SpiceColour) : SpiceColour by colour {
    abstract fun prepareSpice()
}

//Grinding is not unique to curry, or even spices, so we make an interface
//Again, interfaces do not take constructores
interface Grinder{
    fun grind()
}

interface SpiceColour {
    val colour: String
}

//Singleton subclass of SpiceColour
object YellowSpiceColour : SpiceColour {
    override val colour = "yellow"
}

//Create sublcass of Spice class curry, colour defaults to YellowSpiceColour
//TODO review what Grinder is doing here
class Curry(name: String, spiciness: String, colour: SpiceColour = YellowSpiceColour): Spice(name, spiciness, colour), Grinder{
    override fun prepareSpice() {
        TODO("Not yet implemented")
    }

    override fun grind(){

    }
}

fun makeCurry(){
    val curry = Curry("Food", "High")
    println("The ${curry.name} is ${curry.spiciness} in spice")
}
makeCurry()

// * * *  * * * * * * * * Data Classes * * * * * * * * *
//What are data classes? Is it just a way to compare properties more easily?
fun makeDecorations(){

    //Instantiate class
    val d1 = Decorations("granite")
    //Printing a data class prints the actual properties instead of just a pointer to the object
    println(d1)

    val d2 = Decorations("slate")
    println(d2)

    val d3 = Decorations("slate")
    println(d3)

    //Can compare data classes using .equals
    println(d2.equals(d3))

    //Can instantiate a new object with the same property values
    val d4 = d3.copy()
    println(d4)

    val d5 = Decorations2("crystal", "wood", "diver")
    println(d5)
    //Decomposition to assign each variable of object into new variables
    //Number of variables must match the number f properties
    //Variables are assigned in the order of the class
    val (rock, wood, diver) = d5
    println(rock)

}

//Create data class by prefixing with data
//What are other types of classes? Why do we use data?
data class Decorations(val rocks: String){}

//
data class Decorations2(val rocks: String, val wood: String, val diver: String){}

//We can create a data class like normal
data class SpiceContainer(var spice: Spice){
    val label = spice.name
}


fun createSpices(){
    val spiceCabinet = listOf(SpiceContainer(Curry("Yellow Curry", "mild")),
        SpiceContainer(Curry("Red Curry", "medium")),
        SpiceContainer(Curry("Green Curry", "spicy")))

    for(element in spiceCabinet) println(element.label)
}

makeDecorations()
createSpices()


//Special Purpose Classes
//3 other types aside from data
//Singletons (object keyword), use this for any class that shouldn't be instantiated multiple times
//Enums, like below, Kotlin will create one of each colour and there is no way to create more colours
enum class Colour(val rgb: Int){
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF),
    YELLOW(0xFFFF00)
}
//Seal class, class that can only be subclassed in the same file
//This means that Kotlin can see and know all classes and subclasses at compile time, allowing it to do extra checks for you
sealed class Seal

//Subclasses of seal
class SealLion: Seal()
class Walrus: Seal()

//Since all the classes are in the same file we can do unique things
//For instance if we are checking type and don't include all types of seal, kotlin will give a compile error
fun matchSeal(seal: Seal): String{
    //is is a form of typechecking
    return when(seal){
        is Walrus -> "walrus"
        is SealLion -> "sea lion"
        else -> "Other seal"
    }
}

//Example of special classes on spice:
interface SpiceColor1 {
    val colour: Colour
}

object YellowSpiceColor : SpiceColor1 {
    override val colour = Colour.YELLOW
}
