import java.util.*

fun main(args: Array<String>){
    println("Hello, ${args[0]}!")
    feedTheFish()
    val birthday = getBirthday()
    println(getFortuneCookie(birthday))
}

fun dayOfWeek(){
    println("What day is it today?")
    val day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    println( when (day) {
        1 -> "Sunday"
        2 -> "Monday"
        3 -> "Tuesday"
        4 -> "Wednesday"
        5 -> "Thursday"
        6 -> "Friday"
        7 -> "Saturday"
        else -> "Time has stopped"
    })

    //In kotlin all code is an expression (?), that is an if statement will return something
    //Except for while and for loops, and a few others
    val temperature = 10
    println("You are ${ if (temperature > 50) "fried" else "safe"} ")

    //Returns type unit, or no value, we do not need to explicitly say we are returning nothing
}

fun greeting(args: Array<String>){
    //Should catch error if can't convert to int?
    val time = args[0].toInt()
    if (time < 0 || time > 24){
        println("Invalid time")
        return //?
    }

    if (time < 12){
        println("Good Morning Kotlin")
    }else{
        println("Good Night Kotlin")
    }

    //1 line equivalent

    //val result = if (args[0].toInt() < 12) "Good Morning Kotlin" else "Good Night Kotlin"

    println(getResult(args))

}

//Inline function - no curly braces
fun getResult(args: Array<String>) = if (args[0].toInt() < 12) "Good Morning Kotlin" else "Good Night Kotlin"

//Inline v2 - avoid written return and having to specify type.
fun shouldChangeWater(day: String, temperature: Int = 22, dirty:Int = 20) = when {
        temperature > 30 -> true
        dirty > 30 -> true
        day == "Sunday" -> true
        else -> false
    }

fun shouldChangeWater2(day: String, temperature: Int = 22, dirty:Int = 20): Boolean{
    //Without arguments when acts as a series of if statements
    //I.e If temperature
    return when {
        temperature > 30 -> true
        dirty > 30 -> true
        day == "Sunday" -> true
        else -> false
    }
}

//A third method, we can set results inline
fun shouldChangeWater3(day: String, temperature: Int = 22, dirty:Int = 20): Boolean{
    val isTooHot = isTooHot(temperature)
    val isDirty = dirty > 30
    val isSunday = day == "Sunday"
    return when {
        isTooHot -> true
        isDirty -> true
        isSunday -> true
        else -> false
    }
}

//We can have inline fucnitons like this to keep as a placeholder for future implementation
fun getDirtySensorReading() = 20
//Default values can be set from functions
//BUT be careful - these functions are only run at calltime. So if this is a large function it can slow things.
fun shouldChangeWater4(day: String, temperature: Int = 22, dirty:Int = getDirtySensorReading()): Boolean{
    val isTooHot = isTooHot(temperature)
    val isDirty = dirty > 30
    val isSunday = day == "Sunday"
    return when {
        isTooHot -> true
        isDirty -> true
        isSunday -> true
        else -> false
    }
}

//Can turn into inline funcitons
fun isTooHot(temperature: Int): Boolean = temperature > 30

fun feedTheFish(){
    val day = randomDay()
    val food = fishFood(day)
    println("Today is $day and the fish eat $food")

    //Can optionally change default params
    //Prefer specifying dirty = 50 over just 50
    shouldChangeWater(day=day, temperature=23)
    //Don't necessarily need to specify argument
    swim(50, "slow")
}

//We can provide default arguments and mix them with non-default params
fun swim(time: Int, speed: String = "fast"){
    println("swimming $speed for $time")
}


//Need to specify return type or Kotlin will assume void
fun randomDay() : String {
    val week = listOf ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    //Return a random index between 0 and 6, 7 is the boundary
    //Initially got arrayoutofbounds....didn't have enough items in list
    return week[Random().nextInt(7)]
}

//Parameters are specified by name, then return type
fun fishFood(day : String) : String {

    //Since everyhting in Kotlin has a value, when will return the last selected value
    //i.e When day is monday, returns flakes
    return when (day) {
        "Monday" -> "flakes"
        "Tuesday" -> "pellets"
        else -> "fasting"
    }
}

fun canAddFish(tankSize: Double, currentFish: List<Int>, fishSize: Int = 2, hasDecorations: Boolean = true): Boolean{
    /*
    //Initial attempt
    var canAddFish = false
    var decorationImpact = 1.0
    //One-inch-per-fish-per-gallon-of-water,
    //If no decorations total length of fish up to 100% of tank size
    //If decorations, total length of fish <= 80% of tank size
    if (hasDecorations){
        decorationImpact = 0.8
    }

    //Take lower of one inch per gallon
    //Add length of current fish
    if((tankSize * decorationImpact) / (currentFish.sum() + fishSize) >= 1) canAddFish = true

    return canAddFish
    */

    //1 liner equivalent
    return (tankSize * if (hasDecorations) 0.8 else 1.0) >= (currentFish.sum() + fishSize)
}


fun getBirthday() : Int {
    print("Enter your birthday: ")
    //Read user input and convert to int if valid or null otherwise
    //If birthday is null we set it to 0
    return readLine()?.toIntOrNull() ?: 1
}

fun getFortuneCookie(birthday: Int) : String {

    val fortunes = listOf(
        "You will have a great day!",
        "Things will go well for you today.",
        "Enjoy a wonderful day of success.",
        "Be humble and all will turn out well.",
        "Today is a good day for exercising restraint.",
        "Take it easy and enjoy life!",
        "Treasure your friends because they are your greatest fortune."
    )

    /*
    First attempt:
    return when (birthday){
        28, 31 -> fortunes[0]
        in 1..7 -> fortunes[2]
        else -> fortunes[birthday.rem(fortunes.size)]
    }
     */
    //better
    val index = when (birthday){
        28, 31 -> 0
        in 1..7 -> 2
        else -> birthday.rem(fortunes.size)
    }
    return fortunes[index]
}

fun anotherMood(mood: String) = if (mood == "learning") true else false
fun isVeryHot (temperature: Int) = temperature > 35
fun isSadRainyCold (mood: String, weather: String, temperature: Int) =
    mood == "sad" && weather == "rainy" && temperature == 0
fun isHappySunny (mood: String, weather: String) = mood == "happy" && weather == "sunny"

fun whatShouldIDoToday(mood: String, weather: String = "sunny", temperature: Int = 24): String {
    //We do not always need to include arguments for when
    return when{
        isVeryHot(temperature) -> "go swimming"
        isSadRainyCold(mood, weather, temperature) -> "stay in bed"
        isHappySunny(mood, weather) -> "go for a walk"
        anotherMood(mood) -> "Learn about inline functions"
        else -> "Stay home and read."
    }
}

//Filters

//By default filter is eager, meaning it creates a new list with the elements that pass through that filter
fun filterExample() {
    val decorations = listOf("rock", "pagoda", "alligator")

    //The code in {} is in curly braces as we are evaulating the expession for every element in the list
    //Further {} example, random() is assigned at compile time and the value never changes. {random()} has a lambda assigned at compile time and the lambda is executed every time the variable is referenced, returning a different value.

    println(decorations.filter { true })
    //Check if index 0 = character p. Single quotes indicate character, double quotes indicate string.
    println(decorations.filter { it[0] == 'p' })

    //This uses eager behaviour with a new list
    val eager = decorations.filter { it[0] == 'p' }
    //If we want lazy behaviour we use sequences, which can only look at one element at a time
    val lazy = decorations.asSequence().filter { it[0] == 'p' }
    println(eager)
    println(lazy)
    println(lazy.toList())

    //Apply map lazily
    val lazyMap = decorations.asSequence().map {
        println("map: $it")
        it
    }
    println(lazyMap)
    println(lazyMap.first())
    println(lazyMap.toList())


    val spices = listOf("curry", "pepper", "cayenne", "ginger", "red curry", "green curry", "red pepper")

    //filter to sort all spices by string length.
    spices.filter { it.contains("curry") }.sortedBy { it.length }
    //filter starting with c and ending with e
    spices.filter { it.startsWith('c') }.filter { it.endsWith('e') }
    spices.filter { it.startsWith('c') && it.endsWith('e') }

    //Filter first 3 items starting with c
    spices.take(3).filter { it.startsWith('c') }

}

//Lambdas or anonymous functions or function literals
//An expression that makes a function without a name
//Body of function goes after ->
var dirty = 20
val waterFilter = { dirty: Int -> dirty /2}
//We can also declare that water filter takes type int and returns int, then we don't need to declare type in our lambda.
val waterFilter2: (Int) -> Int = {dirty -> dirty/2}
fun feedFish(dirty: Int) = dirty + 10
//Higher order function is any function that takes a function as the argument
//UpdateDirty takes an int and a function that takes an int and returns an int
//Function arguments should be the last paramter
fun updateDirty(dirty: Int, operation: (Int) -> Int): Int{
    return operation(dirty)
}

fun dirtyProcessor(){
    //We can now combine our lambdas to a higher order function,
    //Kind of like "Stacking" functions and passing one after the otther
    dirty = updateDirty(dirty, waterFilter)
    //To pass a function that isn't a lambda we use a double colon so Kotlin knows we are not trying to call it.
    dirty = updateDirty(dirty, ::feedFish)
    dirty = updateDirty(dirty, { dirty -> dirty + 50})
}

//Lambda Practice

//Basic Lambda
//Random().nextInt(n) gets the next random integers value between 0 and n
val rollDice = { Random().nextInt(12) + 1}
//Taking an input
val rollDice2: (Int) -> Int = { sides -> Random().nextInt(sides)+1}
//Equivalent
val rollDice2Equiv = { sides: Int -> Random().nextInt(sides)+1}
//Fix lambda to return 0 if sides = 0
val rollDice3: (Int) -> Int = { sides ->
    if (sides == 0) 0
    else Random().nextInt(sides)+1
}
println(rollDice3(3))

//When to use function type notation instead of just lambda?
//Fyunction type notation is more radable, reducing errors as it shows what type goes in and out

//Example usage of lambdas in a function.
fun gamePlay(diceRoll: Int){
    println("Dice roll is $diceRoll")
}
gamePlay(rollDice())


// * * * * * * * * Driver Code * * * * * * * * * * * *

//greeting(arrayOf("12"))
//dayOfWeek()
//main(arrayOf("Henry"))
//For functions with readline need to right click the file at the top then run filename.
//getFortuneCookie()

canAddFish(10.0, listOf(3,3,3))
canAddFish(8.0, listOf(2,2,2), hasDecorations = false)
canAddFish(9.0, listOf(1,1,3), 3)
canAddFish(10.0, listOf(), 7, true)

whatShouldIDoToday("happy")
println(whatShouldIDoToday("happy", "sunny"))
println(whatShouldIDoToday("sad"))

filterExample()