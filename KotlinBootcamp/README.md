# Android Bootcamp Scratch Files

Holds Scratch files used in Kotlin Bootcamp for programmers.
As per: https://classroom.udacity.com/courses/ud9011

For future readers undertaking the course you may find it easier to start new projects instead of using scratch files the whole time. Feel free to reach out to me with any questions.

## Topics covered
* Android Studio/IntelliJ
* Kotlin Basics (Arrays/Loops, Operators, etc.)
* Functions
* Classes - Private and public classes, Abstract classes and interfaces, data classes
* Annotations, Extentions, Constants, Collections, Generics
* Lambdas, Higher-Order Fucntions, Single Abstract Method

## Extra topics to look at:
* RxJava (To do)
* Unit Testing (To do)
* Dependency Injection (To do)