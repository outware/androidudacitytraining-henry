import kotlin.math.pow

/*
    Useful Tips:
    command d to duplicate line
    control shift up-arrow to shift line of code up
    cmd + / to comment out highlighted lines
    Can shift code output location using output mode in top right next to gradle
 */

fun helloWorld() {
    println("Hello World")
}

helloWorld()

//Kotlin keeps numbers as primitives, however, allows you to call methods on them.
val fish = 3

fish.toLong()

//Integer divided by integer always gives integer
val halfFish = fish/2
val thirdFish = fish/3

println(halfFish)

//We can store numbers as objects, however they need to be wrapped, or "boxed"
val boxed: Number = 1
boxed.toLong()


//Two types of variables
val constant = "Val cannot be changed"
var changable = "Var can be changed"

//Type is inferred (Taken from context), but at compile time is fixed

println("${fish::class.qualifiedName}")
println("${fish::class.simpleName}")
//! Difference between simpleName and qualifiedName?

//Can also specify type
val b: Byte = 1
val i: Int = b.toInt()

//Can use underscores to seperate long constants
val oneMillion = 1_000_000
val hexBytes = 0xFF_FF_FF_FF

//Nullability - Quite unique to Kotlin
//When type is explicitly decalred, by default it cannot be null i.e var rock : Int = null
//Add question mark to allow null
var marbles : Int? = 10
//Allows list elements to be null
var lotsOfFish: List<String?> = listOf(null, null)
var maybeFish: List<String>? = null
//Can combine null
var unknownFish: List<String?>? = null
unknownFish = listOf(null, null)

//Not null assertion operator forces past nullable exception
//TODO review this
//Change marbles between null and 10 to see differbce
println(marbles!!)

/* Come back to
fun test (Int ){

}
*/

//? operator can be used as a "elvis expression" (Basically a ternary" and to check nulls
var fishFoodTreats = 5

//If fishFoodTreats is not null, decrement treats, else return 0
//return fishFoodTreats?.dec() ?: 0

// * * * * * * * * Operators Practice Questions * * * * * * * * *

//Basic Operations:
var fishLeft = 2 + 71 + 233 - 13
//Need +1
var aquariumsNeeded = fishLeft/30 + 1

println("We have $fishLeft fish left")
println("We need $aquariumsNeeded aquariums")

//Alternate (better) solution
2.plus(71).plus(233).minus(13).div(30).plus(1)

//Variables:
var rainbowColour = "Blue"
val blackColour = "Black"
rainbowColour = "Red"
// blackColour = "Blue" won't work

//Nullability:

var greenColour = null
//If type specified must include ? to allow null
var blueColour: String? = null

//Nullability/Lists:
//var list = [null, null] --> Not sure how [] is best used
var list2:List<Int>? = null
var nullFishList: List<String?> = listOf(null, null)
var nullFishList2 = listOf(null, null)


//Null Checks:
var nullTest: Int? = null

//! nullTest?.inc() says if nulLTest is not null increment, if it is null it will be ignored (And not crash the program)
println(nullTest?.inc() ?:0)
//Good to assign a default value, for example if a library has a default value use it, else aplpy my own.

//Use $ to convert variable to string
//Can wrap {} to perform an operation
println("We have ${fish + fishLeft} fish")


//When is the Kotlin equivalent of switches
var welcomeMessage = "Hello and welcome to Kotlin"
when (welcomeMessage.length) {
    0 -> println("Nothing to say?")
    in 1..50 -> println("Perfect")
    else -> println("Too long!")
}

var fishName = "Troust"
when (fishName.length){
    0 -> println("Error with name")
    in 3..12 -> println("Good fish name")
    else -> println("OK")
}

//If referencing val directly it can't change
//But if val references an object that is not immutable the underlying object can still change
//Only the reference is constant

val myList = mutableListOf("tuna","shark","salmon")
myList.remove("shark")
//The list is constant, but it's mutable elements change
//For this reason Immutable != Val

// * * * * * * * * * * * Arrays * * * * * ** * * * * * * * *
val school = arrayOf("tuna","shark","salmon")
val numbers = intArrayOf(1,2,3)
val array2d = arrayOf(school, arrayOf("Dolphin", "Salmon"), "Trout")

//Prints two 2d arrays + trout
println(array2d.contentToString())
println(array2d.asList())


//Initialise Array of size 5, each item is of size index * 2
//! Explanation on this was kind of list, how does {it * 2 initialise}?
val array = Array(5) {it * 2}
println(array.asList())
println(array.contentToString())

// Classic for loop:

for (element in school) println(element)

for ((index, element) in school.withIndex()){
    println("Fish at $index is $element")
}

//Loop of 2d array

//Range works on alphabet, numbers,

for (i in 'b'..'g') print(i)
for (i in 1..5)print(i)
for(i in 5 downTo 1)print(i)
for(i in 1..6 step 2)print(i)

//Example initialisation
val arraySizes = Array(7){ 1000.0.pow(it)}
val sizes = arrayOf("byte", "kilobyte", "megabyte", "gigabyte",
    "terabyte", "petabyte", "exabyte")
for ((i, value) in arraySizes.withIndex()) {
    println("1 ${sizes[i]} = ${value.toLong()} bytes")
}


val numbersArray = listOf(11..15)
val numbersArray2 = listOf(0..100)
//Don't pass mutable lists to functions, always immutable.
//List defaults to immutable
var stringList: MutableList<String> = mutableListOf()
var stringList2: List<String> = listOf()

println(numbersArray2.toString())
for (number in numbersArray){
    println(number)
    stringList.add(number.toString())
}
println(stringList.toString())
