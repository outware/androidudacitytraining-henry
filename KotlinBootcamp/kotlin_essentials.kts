import java.util.*

//Lesson 5 of Udacity

//Pairs
//Kotlin datatype that allow us to define a generic pair of values

//Pair where the first item is mapped to the second
val equipment = "fishnet" to "catching fish"
println(equipment.first)
println(equipment.second)

//We can chain pairs together
val size = "of big size" to "and strong"
val equipment_size = equipment to size

println(equipment_size)
println(equipment_size.first.first)

//Destructure pair
val (tool, use) = equipment
println("The $tool is a tool for $use")

println(equipment.toString())

//Can be used to return from a function
fun giveMeATool(): Pair<String, String> {
    return ("fishnet" to "catching")
}

val (tool1, use1) = giveMeATool()
println(tool1)

//Example use of pairs
class Book(val title: String, val author: String, val year: Int){

    fun authorInfo(): Pair<String, String> {
        return (title to author)
    }

    fun allInfo(): Triple<String, String, Int>{
        return Triple(title, author, year)
    }

}

val newBook = Book("Options, Futures, and Other Derivatives", "John C. Hull", 2017)
println("Here is your book, ${newBook.allInfo().first} written by ${newBook.allInfo().second} in ${newBook.allInfo().third}" )


//Collections

//Remember listOf defaults to being immutable, this is prefereable
//Use mutableListOf where needed
val testList = listOf(11,12,13,14,15,16)

//Easy way to reverse list using kotlin
println(testList.reversed())
//If reversed is not mutable, it returns a new list

//Classic way to reverse a list
fun reverseList(list: List<Int>): List<Int> {
    val result = mutableListOf<Int>()
    for (i in 0..list.size-1){
        result.add(list[list.size-i-1])
    }
    return result
}

//Slightly better Kotlin way to reverse list
fun reverseListAgain(list: List<Int>): List<Int> {
    val result = mutableListOf<Int>()
    for (i in list.size-1 downTo 0){
        result.add(list.get(i))
    }
    return result
}

//List operations
val symptoms = mutableListOf("white","red","green")
symptoms.add("yellow")
symptoms.remove("yellow")

println(symptoms.contains("red"))
//Create a sublist between these indexes
println(symptoms.subList(2,symptoms.size))
listOf(1,2,4).sum()
//If sum doesn't work normally we can provide a lambda to sumOf to sum in a unique way
symptoms.sumOf {it.length}

//Mapping
val sounds = mapOf("loud" to "music", "quiet" to "stream")
sounds.get("loud")
sounds["loud"]
sounds.getOrDefault("rustling", "unknown")
//Get or else, if the key is found it returns it, otherwise it hits the else
sounds.getOrElse("rustling") {"unknown"}
val inventory = mutableMapOf("Fishnet" to 1)
inventory.put("tank scrubber", 3)
inventory.remove("Fishnet")


//Example
val allBooks = setOf("Macbeth", "Romeo and Juliet", "Hamlet", "A Midsummer Night's Dream")
//Map shakespeare to all Books as the author
val library = mapOf("Shakespeare" to allBooks)
println(library)
//Use collection function any
library.any { it.value.contains("Hamlet") }

//Add title/author to moreBooks
val moreBooks = mutableMapOf<String, String>("Wilhelm Tell" to "Schiller")
//If a title is in the map, get it, otherwise add it
//i,e if book jungle book is in here, get it's auther, otherwise put kipling as its author
moreBooks.getOrPut("Jungle Book") { "Kipling" }
moreBooks.getOrPut("Hamlet") { "Shakespeare" }
//Get wilhelm tell author, otherwise make abc
moreBooks.getOrPut("Wilhelm Tell") { "ABC" }
println(moreBooks)

//Constants
//Can make top level constants and assign them a value at compile time using
//const val rocks = 3
val number = 5

//The const value is determined at compile time, with val it is determined at program execution
//So for val we can assign a return value of a funciton as it's value
//Const val is set at compile time
//const only works in the top level and in classes decalared as objects

object Constants {
    const val CONSTANT2 = "object constant"
}

val foo = Constants.CONSTANT2
println(foo)

//To define constants inside a class, they must be wrapped inside a companion object

class MyClass{
    //companion objects are initialised from the static constructuor of the containing class
    //i.e they are initialised when the class is initialised
    companion object{
        const val CONSTANT3 = "Constant inside companion"
    }
}

//Ideally this would be top level const but doesn't work in scratch
val MAX_BOOKS = 5

//An object, we can place constants inside these
object BASE_URL { const val BASE_URL = "Base_URL"}

class Book1(val title: String, val author: String, val year: Int, var pages: Int = 3){

    //If an object is only of interest within our class it makes sense to place them in companion obejct
    companion object{
        const val MAX_NUMBER_BOOKS = 20
        const val BASE_URL = "http://www.turtlecare.net/"
    }

    fun authorInfo(): Pair<String, String> {
        return (title to author)
    }

    fun allInfo(): Triple<String, String, Int>{
        return Triple(title, author, year)
    }

    fun canBorrow(hasBooks: Int): Boolean {
        return (hasBooks < MAX_NUMBER_BOOKS)
    }

    fun printUrl(){
        println(BASE_URL + title +".html")
    }
}


val newBook1 = Book1("Options, Futures, and Other Derivatives", "John C. Hull", 2017)
println(newBook1.printUrl())

//Extension Functions
//A way of declaring util functions
//Adding functions to an existing class wihtout having access to their source code.
//Under the hood extensions do not modify they classes they extend, you just make a new function callable on variables of this type
//Great way to extend classes you don't own and seperate helpers from the core api
//By calling on the string class we are indicating it is an extension
//Since extensino functions are outside the class they do not have access to private variables.
//Extension functions are resolved statically, at compile time
//We can also extend props
fun String.hasSpaces() = find { it == ' '} != null
/*
Equiv to:
fun String.hasSpaces(): Boolean {
    val found = this.find { it == ' '}
    return found != null

 */
fun extensionsExample(){
    "Does it have spaces?".hasSpaces() //true
}

//Example of extension functions
fun Book1.countWeight(): Double{
    return pages*1.5
}

fun Book1.tornPages(tornPages: Int){
    if (pages >= tornPages) pages -= tornPages else pages = 0
}

//Puppy class takes a book as an argument and removes a rnadom number of pages from the book.
class Puppy() {

    fun playWithBook(book: Book1){
        book.tornPages(Random().nextInt(12))
    }

}

val puppy = Puppy()
val book = Book1("Oliver Twist", "Charles Dickens", 1837, 540)

while (book.pages > 0) {
    puppy.playWithBook(book)
    println("${book.pages} left in ${book.title}")
}
println("Sad puppy, no more pages in ${book.title}. ")

//Generics
//A generic type allows us to make a class generic
//If we wanted to define a list class we could make one for each datatype
//Without generics we would need to implement a new copy of list for each type
//With generics we can allow the list to take any type, like a wildcard that accepts any type
//The generic type is T, but you can use any letter or name. T is just a convention.

//Create a list of T
class MyList<T> {

}

//We specify the type for the instance
fun workWithMyList(){
    val intList: MyList<String>
}
