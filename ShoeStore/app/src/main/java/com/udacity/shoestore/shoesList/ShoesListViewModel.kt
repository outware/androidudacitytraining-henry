package com.udacity.shoestore.shoesList

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.models.Shoe

class ShoesListViewModel : ViewModel() {

    // The list of shoes
    private val _shoeList = MutableLiveData<MutableList<Shoe>>()
    val shoeList: LiveData<MutableList<Shoe>>
        get() = _shoeList

    init {
        addDummyShoes()
    }

    //Add shoes
    fun addShoe(newShoe: Shoe) {
        _shoeList.value?.add(newShoe)
    }

    //Update the shoeList with some random shoes
    private fun addDummyShoes() {

        val fakeShoes = mutableListOf<Shoe>(
            Shoe(
                "Air Force 1",
                100.00,
                10.0,
                "Nike",
                "Possibly the most important sneaker in hip hop history (although that’s a story for another day), Air Force 1's are one of Nike’s most popular sneakers of all time. Since 1983, Airs, Uptowns, Forces, or whatever name your city called them, have come in countless colorways, but as any sneakerhead will tell you, nothing compares to a fresh pair of white on white Air Force 1s."
            ),
            Shoe(
                "Stan Smith",
                150.00,
                11.0,
                "Adidas",
                "Named after the American tennis legend, Stan Smith, this sleek and simple design was originally designed for the French tennis star, Robert Haillet. Haillet wore the shoe through the 1960s until he retired in 1971. Stan Smith began wearing the shoe around this time, and eventually, in 1978, the shoe was given a touch of green on the heel and Stan Smith’s name and face on the tongue. For the past 40+ years, the Stan Smith has been a staple for sneakerheads and tennis fans alike."
            ),
            Shoe(
                "Air Jordan 1",
                175.00,
                12.0,
                "Jordan Brand",
                "For many, the Air Jordan 1 is the beginning of the sneakerhead movement. In 1985, Nike laced up Michael “Air” Jordan with his own signature shoe. Whether you’re into the OGs, the Retros, or even the collaborations with creatives like Travis Scott, or Virgil Abloh and his brand Off-White, the Air Jordan 1 is a must-have."
            ),
        )
        _shoeList.value = fakeShoes
    }
}