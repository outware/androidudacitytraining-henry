package com.udacity.shoestore.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.models.Login

class LoginViewModel : ViewModel() {

    private val _loggedIn = MutableLiveData(false)
    val loggedIn: LiveData<Boolean>
        get() = _loggedIn

    //Login check function, if details are correct change state
    fun onLogin(login: Login) {
        //When the login button is pressed we want to take the text in the input fields and compare it against what we expect
        if (login.username == "admin@arq.group" && login.password == "admin") {
            Log.i("onLogin", "Login Success")
            _loggedIn.value = true
        } else {
            _loggedIn.value = false
        }
    }

}