package com.udacity.shoestore.ShoeDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoeDetailBinding
import com.udacity.shoestore.shoesList.ShoesListViewModel
import kotlinx.android.synthetic.main.fragment_add_shoe.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [ShoeDetail.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoeDetail : Fragment() {

    private lateinit var viewModel: ShoesListViewModel
    private lateinit var binding: FragmentShoeDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val args = ShoeDetailArgs.fromBundle(requireArguments())
        val shoeListIndex = args.shoe

        //Inflate View
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shoe_detail, container, false)

        // Get the viewmodel
        viewModel = ViewModelProvider(requireActivity()).get(ShoesListViewModel::class.java)

        val shoe = viewModel.shoeList.value?.get(shoeListIndex)

        //TODO is there a more efficient way?
        //Update view with relevant info
        binding.ShoeName.text = shoe?.name
        binding.shoePrice.text = shoe?.price.toString()
        binding.brand.text = shoe?.company
        binding.detailDescription.text = shoe?.description

        // Inflate the layout for this fragment
        return binding.root
    }
}