package com.udacity.shoestore.models

//Constructor for Login class
data class Login(
    var username: String = "",
    var password: String = ""
)