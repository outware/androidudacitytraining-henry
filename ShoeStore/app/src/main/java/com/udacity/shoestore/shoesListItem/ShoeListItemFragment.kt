package com.udacity.shoestore.shoesListItem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoeListItemBinding

/**
 * A simple [Fragment] subclass.
 * Use the [ShoeListItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoeListItemFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflate View
        val binding = DataBindingUtil.inflate<FragmentShoeListItemBinding>(
            inflater,
            R.layout.shoes_list_fragment,
            container,
            false
        )
        return binding.root
    }
}