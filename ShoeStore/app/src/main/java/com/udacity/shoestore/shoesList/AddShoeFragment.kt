package com.udacity.shoestore.shoesList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentAddShoeBinding
import com.udacity.shoestore.models.Shoe

/**
 * A simple [Fragment] subclass.
 * Use the [AddShoeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddShoeFragment : Fragment() {

    private lateinit var viewModel: ShoesListViewModel
    private lateinit var binding: FragmentAddShoeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflate View
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_shoe, container, false)

        // Get the viewmodel
        viewModel = ViewModelProvider(requireActivity()).get(ShoesListViewModel::class.java)

        binding.addShoe.setOnClickListener { view ->
            addShoe()
            findNavController().navigate(AddShoeFragmentDirections.actionAddShoeFragmentToShoesList())
        }
        return binding.root
    }

    //Add the shoe to the viewModel
    private fun addShoe() {

        //TODO is there a better way to create the Shoe object here?
        //TODO add error checking and enforce field isn't empty
        val newShoe = Shoe(
            binding.name.text.toString(),
            binding.price.text.toString().toDouble(),
            binding.size.text.toString().toDouble(),
            binding.company.text.toString(),
            binding.description.text.toString()
        )
        viewModel.addShoe(newShoe)
    }
}