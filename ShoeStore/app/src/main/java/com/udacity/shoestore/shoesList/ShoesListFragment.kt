package com.udacity.shoestore.shoesList

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoeListItemBinding
import com.udacity.shoestore.databinding.ShoesListFragmentBinding
import com.udacity.shoestore.models.Shoe

class ShoesListFragment : Fragment() {

    private lateinit var viewModel: ShoesListViewModel
    private lateinit var binding: ShoesListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflate View
        binding = DataBindingUtil.inflate(inflater, R.layout.shoes_list_fragment, container, false)

        //Set lifecycle owner
        binding.lifecycleOwner = this.viewLifecycleOwner

        // Get the viewmodel
        viewModel = ViewModelProvider(requireActivity()).get(ShoesListViewModel::class.java)

        binding.addShoe.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_shoesList_to_addShoeFragment)
        }

        // Render shoes
        //If shoe list changes we update the render.
        viewModel.shoeList.observe(viewLifecycleOwner, Observer { shoeList ->
            renderShoes(shoeList)
        })

        return binding.root
    }

    //Loop over all shoes in viewModel and show.
    private fun renderShoes(listOfShoes: MutableList<Shoe>?) {

        if (listOfShoes != null) {
            for ((index, shoe) in listOfShoes.withIndex()) {
                //Inflate view
                val shoeBinding: FragmentShoeListItemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.fragment_shoe_list_item,
                    null,
                    false
                )

                shoeBinding.ShoeName.text = shoe.name
                shoeBinding.shoePrice.text = shoe.price.toString()
                shoeBinding.frameLayout3.setOnClickListener { view ->
                    //Pass an index of shoe
                    view.findNavController()
                        .navigate(ShoesListFragmentDirections.actionShoesListToShoeDetail(index))
                }

                binding.displayShoes.addView(shoeBinding.frameLayout3)
            }
        }
    }
}