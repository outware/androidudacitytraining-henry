package com.udacity.shoestore.login

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.LoginFragmentBinding
import com.udacity.shoestore.models.Login
import timber.log.Timber

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflate View
        val binding = DataBindingUtil.inflate<LoginFragmentBinding>(inflater, R.layout.login_fragment, container, false)

        // Get the viewmodel
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.logInButton.setOnClickListener {
            val login = Login (binding.username.text.toString(), binding.password.text.toString())
            viewModel.onLogin(login)
        }

        //Get result of login attempt
        viewModel.loggedIn.observe(viewLifecycleOwner, Observer {
            if (it) Toast.makeText(activity?.applicationContext, "Login Successful" , Toast.LENGTH_SHORT).show()
            else Toast.makeText(activity?.applicationContext, "Login Unsuccessful" , Toast.LENGTH_SHORT).show()
        })

        //Navigate to next screen
        // Sets up event listening to navigate the player when the game is finished
        viewModel.loggedIn.observe(viewLifecycleOwner, Observer { isLoggedIn ->
            if (isLoggedIn) {
                val action = LoginFragmentDirections.actionLoginFragmentToWelcomeFragment()
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        return binding.root
    }
}